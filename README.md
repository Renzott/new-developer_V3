## How to run the code

## Requirements
- pnpm
- node v20

## Install dependencies
pnpm install

## Run the code

pnpm start


## How to run the tests

pnpm test

## How to use

### Docker

The rendered markdown should be shown in the console.
You can modify the delay time in the file: **docker-compose.yml** with the environment: **DELAY**


### Node Local

You can move between markdowns with:

- "Enter" key to next markdown
- "c" key to previous markdown
- "q" key to exit