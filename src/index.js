import { marked } from 'marked';
import { markedTerminal } from 'marked-terminal';
import { exit, stdin } from 'process';
import { emitKeypressEvents } from 'readline'
import { readMD, sleep } from './utils/index.js';
import { env } from "process"

const mdFiles = [
    './src/answers/q1/1.md',
    './src/answers/q2/2.md',
    './src/answers/q3/3.md',
    './src/answers/q4/4.md',
    './src/answers/q5/5.md',
]

emitKeypressEvents(stdin);
marked.use(markedTerminal([]));

let index = 0;

readMD(mdFiles[index]);

if (process.stdin.isTTY) {
    stdin.setRawMode(true);
} else{
    for (let index = 0; index < mdFiles.length; index++) {
        readMD(mdFiles[index]);
        const delay = env.DELAY || 20000;
        await sleep(delay);
    }
}


stdin.on('keypress', (_, key) => {
    if (index < mdFiles.length - 1 && key.name === 'return') {
        index++;
        readMD(mdFiles[index]);
    } else if (index > 0 && key.name === 'c') {
        index--;
        readMD(mdFiles[index]);
    } else if (key.name === 'q' || key.ctrl && key.name === 'c' ) {
        exit();
    }
});



