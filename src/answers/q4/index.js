const findPairWithSum = (arr, sum) => {

    let hashMap = {};
    const result = [];

    for (let index = 0; index < arr.length; index++) {
        const currentNumber = arr[index]

        if (hashMap[currentNumber]) {
            result.push([currentNumber, sum - currentNumber])
        }
        else {
            hashMap[sum - currentNumber] = true
        }

    }
    return {
        result: result.length > 0,
        pair: result
    }

}

export default findPairWithSum;