import { readFile } from "fs";
import { marked } from "marked";

export function readMD(path) {
    readFile(path, 'utf8', (_, data) => {
        consoleClear()
        console.log(marked(data.toString()));
    });
}

export function consoleClear() {
    console.log('\x1b[2J');
}

export function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
