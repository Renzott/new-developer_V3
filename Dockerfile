### node 20

FROM node:20-alpine

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
COPY package*.json ./

# Install dependencies
RUN npm install -g pnpm
RUN pnpm install

# Bundle app source
COPY . .

# Build app
ENTRYPOINT [ "pnpm", "start" ]