import { writeFile } from 'fs';

const array = [];

for (let i = 0; i < 10000; i++) {
    const number = Math.floor(Math.random() * 10000) + 1;
    if (array.indexOf(number) === -1) {
        array.push(number);
    }
}

array.sort((a, b) => a - b);

writeFile('test/arrayNumbers.json', JSON.stringify(array), (err) => {
    if (err) throw err;
    console.log('The file has been saved!');
});