import { equal } from 'assert';
import findPairWithSum from '../src/answers/q4/index.js';
import arrayNumbers from "./arrayNumbers.json" assert { type: "json" };

describe('Q4 Test', () => {
    it('should return true and the pair', () => {
        const result = findPairWithSum([1, 2, 3, 4, 5], 7);
        equal(result.result, true);
    });

    it('should return false and the pair', () => {
        const result = findPairWithSum([1, 2, 3, 4, 5], 10);
        equal(result.result, false);
    });

    it('should return true and the pair', () => {
        const result = findPairWithSum(arrayNumbers, 100);
        equal(result.result, true);
    });

});